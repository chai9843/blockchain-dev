import json
from web3 import Web3, HTTPProvider
from web3.contract import ConciseContract


# compile your smart contract with truffle first
truffleFile = json.load(open('/Users/chaitanyavarmamudundi/Desktop/Json_to_block/build/contracts/database.json'))
abi = truffleFile['abi']
bytecode = truffleFile['bytecode']

# web3.py instance
w3 = Web3(HTTPProvider("HTTP://127.0.0.1:7545"))
print(w3.isConnected())
contract_address = Web3.toChecksumAddress("0x30A93c1578961F05F688684c6588B83c002F0631")

# Instantiate and deploy contract
contract = w3.eth.contract(abi=abi, bytecode=bytecode)
# Contract instance
contract_instance = w3.eth.contract(abi=abi, address=contract_address)
# Contract instance in concise mode
#contract_instance = w3.eth.contract(abi=abi, address=contract_address, ContractFactoryClass=ConciseContract)

# Getters + Setters for web3.eth.contract object ConciseContract
#print(format(contract_instance.getGreeting()))

print('Contract value: {}'.format(contract_instance.functions.transaction(1).call()))
