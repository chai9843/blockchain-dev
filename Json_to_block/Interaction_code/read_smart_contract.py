"""
- This code reads a smart contract.
- To read a smart contract 2 things are required(smart-contract address and smart-contract ABI)
- the abi follows ERC-20 specific constraints
- I am using "OMG Network" for testing
"""
#connecting to smart contracts

import json
from web3 import Web3
#infura_url="https://mainnet.infura.io/v3/5d098af92936443897f724da9e008e52"
trans_id=1
trans_type="pay"
amount1="40"
amount2="5.6"
url="HTTP://127.0.0.1:7545"
web3=Web3(Web3.HTTPProvider(url))
compiled_contract_path = '/Users/chaitanyavarmamudundi/Desktop/Json_to_block/build/contracts/database.json'
deployed_contract_address = '0x82cc364734E914010d2DfA00dC14c73c921657f8'
with open(compiled_contract_path) as file:
    contract_json = json.load(file)  # load contract info as JSON
    contract_abi = contract_json['abi']  # fetch contract's abi - necessary to call its functions
contract = web3.eth.contract(address=deployed_contract_address, abi=contract_abi)
addtrans=contract.functions.addTrans(1,trans_type.encode(),amount1.encode(),amount2.encode()).call()
trans=contract.functions.transaction(1).call()
print(addtrans,trans)
