import json
from web3 import Web3, HTTPProvider
from web3.contract import ConciseContract


# compile your smart contract with truffle first
truffleFile = json.load(open('/Users/chaitanyavarmamudundi/Desktop/Json_to_block/build/contracts/database.json'))
abi = truffleFile['abi']
bytecode = truffleFile['bytecode']

# web3.py instance
w3 = Web3(HTTPProvider("HTTP://127.0.0.1:7545")) #modify
print(w3.isConnected())
contract_address = Web3.toChecksumAddress("0x30A93c1578961F05F688684c6588B83c002F0631") #modify
key="fb2d8e0397940e9a1e2ba3ee91f42c1f6e36a820743c9380961db6ec1d5c4390" #modify
acct = w3.eth.account.privateKeyToAccount(key)
account_address= acct.address

# Instantiate and deploy contract
contract = w3.eth.contract(abi=abi, bytecode=bytecode)
# Contract instance
contract_instance = w3.eth.contract(abi=abi, address=contract_address)
# Contract instance in concise mode
#contract_instance = w3.eth.contract(abi=abi, address=contract_address, ContractFactoryClass=ConciseContract)

tx = contract_instance.functions.addTrans(2,"PAY","40.5","90").buildTransaction({'nonce': w3.eth.getTransactionCount(account_address)})
#Get tx receipt to get contract address
signed_tx = w3.eth.account.signTransaction(tx, key)
#tx_receipt = w3.eth.getTransactionReceipt(tx_hash)
hash= w3.eth.sendRawTransaction(signed_tx.rawTransaction)
print(hash.hex())
