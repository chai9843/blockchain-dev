pragma solidity ^ 0.5.16;

contract Database{
    //Mappings
    mapping(uint => Trans) public transaction;
    mapping(uint => mapping(uint => Item)) public transaction_item;
    //Struct for transaction
    struct Trans{
        string paymenttype;
        string subtotal;
        string totalprice;
    }
    
    struct Item{
        string itemname;
        string category;
        string quantity;
        string itemprice;
    }
    
    //function add trans
    function addTrans(uint _transid,string memory _paymenttype,string memory _subtotal,string memory _totalprice) public {
        transaction[_transid]=Trans(_paymenttype,_subtotal,_totalprice);
    }
    
    function addTransItem(uint _transid, uint _item_id,string memory _itemname, string memory _category, string memory _quantity, string memory _itemprice) public {
        transaction_item[_transid][_item_id]=Item(_itemname,_category,_quantity,_itemprice);
    }
}